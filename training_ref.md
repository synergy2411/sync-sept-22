# Generate Package.json file

    > npm init [-y]

    > npm i colors

# Node Installer -

- Node Runtime Environment
- Node Package Manager (NPM)
- Node Core/Native Module
  : eg. http, fs, os, events, process, path etc

# require("colors")

> NOT a file module
> search for node_modules till it reaches to root of the project
> either the module will be file or it will be a folder
> file -> return it
> folder -> look for package.json -> contain main property -> will return the file

# Notes App

> node src\index.js add --title="New Title" --body="New Title Body"
> node index.js read --title="Some Title"
> node index.js remove --title="Some Title"
> node index.js list

> External Module -> yargs, colors
> Native -> FS -> access physical file storage
> File Module -> for add, read, remove and list the notes

# HTTP Module

# Semantic Versioning : X.Y.Z (semver.org)

-> X - Major Version : new functionality, not compatible with existing code base
-> Y - Minor Version : new functionality added in existing project/code base
-> Z - Patch Version : bug fix & optimization technique

1.0.0
1.1.0
1.1.1
2.1.0
2.2.0 -> installed version
2.2.1
2.2.2
3.0.1

> npm outdated
> Current : 2.2.0
> Wanted : 2.2.2
> Latest : 3.0.1

> npm update -> update to maximum patch version

# Create your account on NPMJS.COM

> npm adduser
> npm publish
> npm unpublish
> npm config get registry
> npm install
> npm uninstall
> npm init
> npm outdated
> npm update
> npm search

# Events / PubSub

- EventEmitter Class
- emit()
- on()

# REST API - CRUD operation

- GET : /api/todos (fetch all items)
- GET : /api/todos/:todoId (fetch single element on the resource)
- POST: /api/todos + Request Body (create new resource)
- PATCH: /api/todos/:todoId + Body (update the existing resource)
- DELETE: /api/todos/:todoId (delete the single resource)

# MongoDB Driver

> npm install mongodb

# Mongoose

- Schema based solution
- Validation
- hooks - pre / post

- populate()

# Template View / View Engine / View

- inject dynamic values in template

# Socket Programming

# Authentication

- JWT
- PassportJS

> Mongoose - DB Connection
> passport
> passport-local
> express
> express-session
> connect-mongo
> ejs
> dotenv

# Deployment Steps

- Create Account on Heroku
- Download Heroku CLI tool
- Upload entrie project on git
- heroku commands

  > heroku login
  > heroku keys:add

  - optional SSH - powershell "ssh-keygen" > ssh-key in ~/.ssh
    > heroku create <unique-project-name>
    > git remote
    > git push heroku main

- put - remove this existing object and create new one
  eg. {username : "", password : ""}
- patch - update the existing element
  eg. {username : "" , password : ""}

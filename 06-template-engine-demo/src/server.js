const express = require("express");

const app = express();

const PORT = process.env.PORT || 9090;

const user = { name: "John Doe" }

// const username = "John Doe";

const isAuthenticated = false;

const friends = ["Jenny", "Jack", "Jill"]

// app.set("view engine", "pug");
app.set("view engine", "ejs");

app.set("views", "./src/views");

app.get("/", (req, res) => {
    res.render("index")
})


app.get("/users", (req, res) => {
    res.render("users", { user, isAuthenticated, friends })
})




app.listen(PORT, () => console.log("Server started at PORT : " + PORT))
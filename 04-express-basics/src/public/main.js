const btnAdd = document.querySelector("#btnAdd");
const txtInput = document.querySelector("#txtInput");
const listContainer = document.querySelector("#list-container");
let todos = [];

const createTodo = (label) => {
    fetch("http://localhost:9090/api/todos", {
        method: "POST",
        body: JSON.stringify({ label }),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then((result) => {
            console.log(result);
            todos.push(result);
            createUI();
        })
        .catch(console.error);
};

btnAdd.addEventListener("click", function (event) {
    createTodo(txtInput.value);
});

const createUI = () => {
    listContainer.innerHTML = "";
    todos.forEach((todo) => {
        const liEl = document.createElement("li");
        liEl.innerHTML = `
          ${todo.label}
      `;
        listContainer.appendChild(liEl);
    });
};

const fetchTodos = () => {
    fetch("/api/todos")
        .then((response) => response.json())
        .then((result) => {
            todos = result;
            createUI();
        })
        .catch(console.error);
};

fetchTodos();
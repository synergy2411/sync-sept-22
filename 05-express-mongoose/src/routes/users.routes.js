
const express = require("express");
const router = express.Router();
const { fetchUsers, createUser, userLogin, deleteUser, updatedUser } = require("../controller/users.controller");

router.route("/")
    .get(fetchUsers)
    .post(createUser)
router.route("/login")
    .post(userLogin)
router.route("/:userId")
    .delete(deleteUser)
    .patch(updatedUser)

module.exports = router;
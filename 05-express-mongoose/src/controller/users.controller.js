const UserModel = require("../model/users.model");
const { hashSync, compareSync } = require("bcrypt")

const fetchUsers = async (req, res) => {
    try {
        const allUsers = await UserModel.find();
        const filteredUser = allUsers.map(user => {
            return { ...user._doc, password: null }
        })
        return res.send(filteredUser);
    } catch (err) {
        return res.send({ error: "Something went wrong" })
    }
}

const createUser = async (req, res) => {
    const { username, email, password } = req.body
    try {
        const foundUser = await UserModel.findOne({ email })
        if (foundUser) {
            return res.send({ error: "User already exist" })
        } else {
            const hashedPassword = hashSync(password, 12)
            let user = {
                username, email: email.toLowerCase(),
                password: hashedPassword
            }
            let newUser = new UserModel(user)
            const createdUser = await newUser.save()
            return res.send({ ...createdUser._doc, password: null })
        }
    } catch ({ errors }) {
        return res.send({ message: errors.email.message })
    }
}

const deleteUser = async (req, res) => {
    const { userId } = req.params;
    try {
        const deleteResult = await UserModel.findByIdAndDelete(userId)
        if (deleteResult) {
            return res.send(deleteResult)
        } else {
            return res.send({ error: "User NOT found - " + userId })
        }
    } catch (err) {
        console.log(err);
        return res.send(err)
    }
}

const updatedUser = async (req, res) => {
    const { oldPassword, newPassword } = req.body;
    const { userId } = req.params;
    try {
        const foundUser = await UserModel.findById(userId)
        if (foundUser) {
            const isMatch = compareSync(oldPassword, foundUser._doc.password)
            if (isMatch) {
                const hashedNewPassword = hashSync(newPassword, 12);
                const updatedUser = await UserModel.findByIdAndUpdate(userId, { password: hashedNewPassword })
                return res.send(updatedUser)
            } else {
                return res.send({ err: "BAD CREDENTIALS" })
            }
        } else {
            return res.send({ err: "Unable to locate user - " + userId })
        }
    } catch (err) {
        return res.send({ err: "Somethign went wrong" })
    }
}

const userLogin = async (req, res) => {
    const { email, password } = req.body;
    try {
        const foundUser = await UserModel.findOne({ email })
        if (foundUser) {
            const isMatch = compareSync(password, foundUser._doc.password)
            if (isMatch) {
                return res.send({ message: "SUCCESS" })
            } else {
                return res.send({ message: "BAD CREDENTIALS" })
            }
        } else {
            return res.send({ error: "Unable to locate user email - " + email })
        }
    } catch (err) {
        return res.send({ error: "Something went wrong" })
    }
}


module.exports = {
    fetchUsers,
    createUser,
    deleteUser,
    updatedUser,
    userLogin
}
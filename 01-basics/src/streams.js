const fs = require("fs");
const { request } = require("http");

const readStream = fs.createReadStream("./src/test.txt")

const writeStream = fs.createWriteStream("./src/test2.txt")

// readStream.on("data", (chunk) => {
//     writeStream.write(chunk)
// })

// readStream.on("end", () => {
//     console.log("completely read");
// })

readStream.pipe(writeStream);

// Echo Server
// request.pipe(response);

// Upload Server
// request.pipe(writeStream);

// Download Server
// readStream.pipe(response);
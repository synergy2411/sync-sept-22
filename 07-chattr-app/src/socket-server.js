const express = require("express");
require("./db");
const app = express()
const http = require("http");
const server = http.createServer(app)
const io = require("socket.io")(server)

const ChatModel = require("./model/chat.model");

const messageStack = [];

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/public/client.html")
})

io.on("connection", (client) => {
    console.log("Client Connected");
    client.emit("acknowledge", "You are connected now!")
    client.on("MessageToServer", (chatterName, message) => {
        console.log(chatterName + " says :", message)
        client.emit("MessageToClient", 'Me', message)
        client.broadcast.emit("MessageToClient", chatterName, message);
        messageStack.push({ chatterName, message })
    })
    client.on("disconnect", async () => {
        try {
            const chatData = new ChatModel({ messages: messageStack })
            const savedData = await chatData.save()
            console.log(savedData);
        } catch (err) {
            console.log(err);
        }
    })
})


server.listen(9000, () => console.log("Socket Server running on PORT : 9000"))


// EventEmitter -> on / emit
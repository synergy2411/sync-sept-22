const { Schema, model } = require("mongoose")

const chatSchema = new Schema({
    messages: [{ chatterName: Schema.Types.String, message: Schema.Types.String }]
})

module.exports = model("Chat", chatSchema)
require("./db");
const express = require("express");
const UserRouter = require("./routes/users.routes");

const app = express();

app.use(express.json());

app.use("/api/users", UserRouter);

app.listen(9001, () => console.log("Server started at PORT : 9001"))
const http = require("http");
const fs = require("fs").promises;

const server = http.createServer(function (request, response) {
    console.log("URL : ", request.url);

    // JSON CONTENT
    // response.setHeader("Content-Type", "application/json")
    // response.writeHead(200);
    // response.write(JSON.stringify({ message: "SUCCESS" }))

    // HTML CONTENT
    // response.setHeader("Content-Type", "text/html")
    // response.writeHead(200);
    // response.write(`
    //     <html>
    //         <head></head>
    //         <body>
    //             <h1>Hello from Server</h1>
    //         </body>
    //     </html>
    // `)

    // HTML FILE
    // response.setHeader("Content-Type", "text/html")
    // response.writeHead(200);
    // fs.readFile("./src/public/index.html")
    //     .then(content => {
    //         response.write(content);
    //         response.end();
    //     })
    //     .catch(error => {
    //         response.write(`
    //             <h4>Something bad happened</h4>
    //         `)
    //         response.end();
    //     })

    const posts = [
        { id: "p001", title: "Awesome post" },
        { id: "p002", title: "Nice post" },
        { id: "p003", title: "Great post" },
    ]

    const notes = [
        { id: "n001", body: "Great Note" },
        { id: "n002", body: "Like it" }
    ]

    switch (request.url) {
        case "/posts": {
            response.write(JSON.stringify(posts))
            response.end();
            break;
        }
        case "/notes": {
            response.write(JSON.stringify(notes))
            response.end();
            break;
        }
        default: {
            response.write(JSON.stringify({ error: "No route found" }))
            response.end();
        }
    }



})

server.listen(9090, "localhost", () => console.log(`Server started at http://localhost:9090`));
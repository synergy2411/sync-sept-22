const { Schema, model } = require("mongoose");

const userSchema = new Schema({
    username: {
        type: Schema.Types.String,
        required: true
    },
    email: {
        type: Schema.Types.String,
        required: true,
        validate: {
            validator: value => {
                return value.includes("@")
            },
            message: () => "Email should be in proper format"
        }
    },
    password: {
        type: Schema.Types.String,
        required: true
    }
})

userSchema.pre("save", () => {
    console.log("Pre - Save")
})

userSchema.post("save", () => {
    console.log("Post - save")
})

const UserModel = model("Users", userSchema)

module.exports = UserModel;

// let user = new UserModel({ username: "alice doe", email: "alice@test", password: "alice123" })

// user.save().then(result => {
//     console.log(result);
// }).catch(console.log)

// UserModel.find().then(docs => console.log(docs)).catch(console.log)

// User -> users
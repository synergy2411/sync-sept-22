const { exit } = require("process");

const EventEmitter = require("events").EventEmitter;

let emitter = new EventEmitter();

// to Emit the event - emit()
// tp Subscribe the event - on()

const subsriberOne = () => {
    console.log("Sub 01");
}

const subscriberTwo = () => {
    console.log("Sub 02");
}

emitter.setMaxListeners(25);            // default listener count 11

for (let i = 0; i < 20; i++) {
    emitter.on("EventOne", subsriberOne)
}
emitter.once("EventOne", subscriberTwo)

emitter.on("EventOne", () => {
    console.log("Event One Fired");
    // emitter.removeListener("EventOne", subsriberOne);
})


emitter.emit("EventOne")
// emitter.emit("EventOne")



// process.on("uncaughtException", () => {
//     console.log("Exception catched");
// })

// doesNotExist();

// process.on("exit", (code) => {
//     console.log("About to exit with code ", code);
// })

// exit(1)

const express = require("express");

const app = express();

app.use(express.json());            // Parse req body and attach it with req object

app.use(express.static(__dirname + "/public"));       // to serve the static contents


let todos = [
    { id: "t001", label: "to buy pules", status: false },
    { id: "t002", label: "to shop jenas", status: true },
    { id: "t003", label: "to renew car insurance", status: false },
]



// GET -> http://localhost:9090
app.get("/", (req, res) => {
    res.sendFile(__dirname + "/public/index.html")
})

app.patch("/api/todos/:todoId", (req, res) => {
    const { todoId } = req.params;
    const position = todos.findIndex(todo => todo.id === todoId)
    if (position >= 0) {
        todos[position] = { ...todos[position], ...req.body }
        return res.send(todos[position]);
    } else {
        return res.send({ error: "Item NOT found for " + todoId })
    }
})

// DELETE -> http://localhost:9090/:todoId
app.delete("/api/todos/:todoId", (req, res) => {
    const { todoId } = req.params;
    const position = todos.findIndex(todo => todo.id === todoId)
    if (position >= 0) {
        const itemToDelete = todos[position];
        todos = todos.filter(todo => todo.id !== todoId);
        return res.send({ message: "Item deleted for " + todoId, deletedItem: itemToDelete })
    } else {
        return res.send({ error: "Item NOT found for " + todoId })
    }
})

// POST -> http://localhost:9090/api/todos + Req Body
app.post("/api/todos", (req, res) => {
    if (req.body && req.body.label) {
        let item = {
            label: req.body.label,
            status: false,
            id: "t00" + (todos.length + 1)
        }
        todos.push(item)
        return res.send(item)
    } else {
        return res.send({ error: "Request body NOT found" })
    }
})

// GET -> http://localhost:9090/api/todos/t003 (Route Parameter)
app.get("/api/todos/:todoId", (req, res) => {
    const { todoId } = req.params;
    const todoItem = todos.find(todo => todo.id === todoId);
    if (todoItem) {
        return res.send(todoItem)
    } else {
        return res.send({ error: "Item NOT found for " + todoId })
    }
})

// GET -> http://localhost:9090/api/todos?limit=2
app.get("/api/todos", (req, res) => {
    const { limit } = req.query;
    if (limit) {
        const filteredTodo = todos.filter((value, index) => index < limit)
        return res.send(filteredTodo)
    } else {
        return res.send(todos)
    }
})




// app.use(function (req, res, next) {
//     console.log("Method : ", req.method);
//     console.log("URL : ", req.url);
//     // res.send("Hell from Express server");
//     next();
// }, function (req, res, next) {
//     res.send("Response from second middleware")
// })

app.listen(9090, () => console.log("Express server started at PORT : 9090"));
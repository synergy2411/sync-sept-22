require("dotenv").config();

const { mongoUser, mongoPassword } = process.env;

const MongoClient = require("mongodb").MongoClient;

const mongoURL = "mongodb://localhost:27017"

const mongoAtlasURL = `mongodb+srv://${mongoUser}:${mongoPassword}@cluster0.e9xsq.mongodb.net/?retryWrites=true&w=majority`

let db, usersCollection;

MongoClient.connect(mongoAtlasURL)
    .then(conn => {
        console.log("Mongo Connected");
        db = conn.db("sync-db");
        usersCollection = db.collection("users");
        // findUsers();
        insertUser()
    })
    .catch(console.log)

const findUsers = () => {
    usersCollection.find().toArray().then(docs => {
        console.log(docs)
    }).catch(console.log)
}

const insertUser = () => {
    usersCollection.insertOne({ username: "Jenny Doe", email: "jenny@test", password: "jenny123" })
        .then(result => console.log(result))
        .catch(console.log)
}
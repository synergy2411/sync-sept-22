const fs = require("fs");
require("colors");

const loadNotes = () => {
    try {
        const bufferNotes = fs.readFileSync("./src/my-notes.json")
        return JSON.parse(bufferNotes.toString())
    } catch (err) {
        return [];
    }
}

const saveNotes = (notes) => {
    fs.writeFileSync("./src/my-notes.json", JSON.stringify(notes));
    console.log("Notes Saved".green);
}

const addNote = (title, body) => {
    const allNotes = loadNotes();
    const foundNote = allNotes.find(note => note.title === title)
    if (!foundNote) {
        allNotes.push({ title, body });
        saveNotes(allNotes);
    } else {
        console.log("Duplicate title. Try Again!".red);
    }
}

const readNote = (title) => {
    const allNotes = loadNotes();
    const foundNote = allNotes.find(note => note.title.toLowerCase() === title.toLowerCase())
    if (foundNote) {
        console.log("Title : ", foundNote.title);
        console.log("Body : ", foundNote.body);
    } else {
        console.log("Note NOT found".red);
    }
}

const removeNote = title => {
    let allNotes = loadNotes();
    let position = allNotes.findIndex(note => note.title === title);
    if (position >= 0) {
        allNotes = allNotes.filter(note => note.title !== title)
        saveNotes(allNotes);
        console.log("Removed - ", title);
    } else {
        console.log("Title does not exist. Try Again!".red);
    }
}

const listNotes = () => {
    const allNotes = loadNotes();
    console.log("______________".blue);
    console.log("All Notes".blue);
    console.log("______________".blue);
    allNotes.forEach(note => {
        console.log("______________".grey);
        console.log("Title : ", note.title);
        console.log("Body : ", note.body);
    })
}

module.exports = {
    addNote,
    readNote,
    removeNote,
    listNotes
}
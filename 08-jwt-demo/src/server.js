const express = require("express");
const jwt = require("jsonwebtoken");

const app = express();
const SECRET_KEY = "My Super Secret Key";

app.use(express.json());        // "application/json"
app.use(express.urlencoded({ extended: false }));            // "URL Encoded Content Type"

const users = [];

app.post("/users/register", (req, res) => {
    if (req.body) {
        const { username, password } = req.body;
        const newUser = {
            username, password, id: Date.now().toString()
        }
        users.push(newUser);
        return res.send(newUser);
    } else {
        return res.send({ message: "Need to supply username and password" });
    }
})

app.post("/users/login", (req, res) => {
    if (req.body) {
        const { username, password } = req.body;
        const foundUser = users.find(user => user.username === username && user.password === password);
        if (foundUser) {
            const token = jwt.sign({ id: foundUser.id, username: foundUser.username }, SECRET_KEY)
            return res.send({ token })
        } else {
            return res.send({ error: "BAD CREDENTAILS" })
        }
    }
})


const ensureToken = (req, res, next) => {
    const authHeaders = req.headers.authorization;
    if (authHeaders) {
        const token = authHeaders.split(" ")[1]         // Bearer <TOKEN_VALUE>
        if (token) {
            req.token = token;
            next();
        } else {
            return res.send({ error: "Token NOT found" })
        }
    } else {
        return res.send({ error: "Auth Required" })
    }

}

app.post("/users/change-password", ensureToken, (req, res) => {
    try {
        const { oldPassword, newPassword } = req.body;
        const decode = jwt.verify(req.token, SECRET_KEY)            // {iat, id, username}
        const { id } = decode;
        const position = users.findIndex(user => user.id.toString() === id.toString())
        if (position >= 0) {
            users[position].password = newPassword;
            return res.send({ message: "PASSWORD CHANGED" })
        } else {
            return res.send({ message: "Token Modified" })
        }
    } catch (err) {
        return res.send({ err })
    }
})

app.get("/users", (_, res) => res.send(users))

app.listen(9001, () => console.log("Server started at PORT : 9001"));
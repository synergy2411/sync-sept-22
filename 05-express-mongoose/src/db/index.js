const { connect } = require("mongoose");
require("dotenv").config();

let URI = process.env.mongoURI

connect(URI)
    .then(conn => console.log("Mongo Connected!!"))
    .catch(console.log)